package com.example.demo.controllers;

import com.example.demo.TestUtils;
import com.example.demo.model.persistence.Cart;
import com.example.demo.model.persistence.Item;
import com.example.demo.model.persistence.User;
import com.example.demo.model.persistence.repositories.CartRepository;
import com.example.demo.model.persistence.repositories.ItemRepository;
import com.example.demo.model.persistence.repositories.UserRepository;
import com.example.demo.model.requests.CreateUserRequest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ItemControllerTest {

    private ItemController itemController;

    private ItemRepository itemRepo = mock(ItemRepository.class);



    @Before
    public void setUp() {
        itemController = new ItemController();
        TestUtils.injectObjects(itemController, "itemRepository", itemRepo);

        List<Item> items = new ArrayList<>();
        List<Item> item1l = new ArrayList<>();

        Item item1 = new Item();
        item1.setId(1L);
        item1.setName("item1");
        item1.setDescription("hello_item1");
        item1.setPrice(BigDecimal.valueOf(1));

        Item item2 = new Item();
        item2.setId(2L);
        item2.setName("item2");
        item2.setDescription("hello_item2");
        item2.setPrice(BigDecimal.valueOf(2));

        items.add(item1);
        items.add(item2);
        item1l.add(item1);

        when(itemRepo.findById(1L)).thenReturn(Optional.of(item1));
        when(itemRepo.findByName("item1")).thenReturn(item1l);
        when(itemRepo.findAll()).thenReturn(items);
    }

    @Test
    public void testGetItems() throws Exception {

        final ResponseEntity<List<Item>> response = itemController.getItems();

        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());

        List<Item> listItems = response.getBody();
        assertNotNull(listItems);
        assertEquals(2, listItems.size());
    }

    @Test
    public void testGetItemById() throws Exception {

        final ResponseEntity<Item> response = itemController.getItemById(1L);

        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());

        Item i = response.getBody();
        assertNotNull(i);
        assertEquals(1L, i.getId());
        assertEquals("item1", i.getName());
        assertEquals("hello_item1", i.getDescription());
        assertEquals(BigDecimal.valueOf(1), i.getPrice());
    }

    @Test
    public void negative_test_no_id() throws Exception {

        final ResponseEntity<Item> response = itemController.getItemById(10L);

        assertNotNull(response);
        assertEquals(404, response.getStatusCodeValue());
    }

    @Test
    public void testGetItemsByName() throws Exception {

        final ResponseEntity<List<Item>> response = itemController.getItemsByName("item1");

        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());

        List<Item> listItems = response.getBody();
        assertNotNull(listItems);
        assertEquals(1, listItems.size());
    }
}
