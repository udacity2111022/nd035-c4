package com.example.demo.controllers;

import com.example.demo.TestUtils;
import com.example.demo.model.persistence.Cart;
import com.example.demo.model.persistence.Item;
import com.example.demo.model.persistence.User;
import com.example.demo.model.persistence.UserOrder;
import com.example.demo.model.persistence.repositories.CartRepository;
import com.example.demo.model.persistence.repositories.ItemRepository;
import com.example.demo.model.persistence.repositories.OrderRepository;
import com.example.demo.model.persistence.repositories.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OrderControllerTest {

    private OrderController orderController;

    private UserRepository userRepo = mock(UserRepository.class);

    private OrderRepository orderRepo = mock(OrderRepository.class);



    @Before
    public void setUp() {
        orderController = new OrderController();
        TestUtils.injectObjects(orderController, "orderRepository", orderRepo);
        TestUtils.injectObjects(orderController, "userRepository", userRepo);


        User user = new User();
        user.setId(1L);
        user.setUsername("Username1234");
        user.setPassword("Username1234");

        Item item = new Item();
        item.setId(1L);
        item.setName("item1");
        item.setDescription("hello_item");
        item.setPrice(BigDecimal.valueOf(1));

        List<Item> items = new ArrayList<>();
        items.add(item);

        Cart cart = new Cart();
        cart.setId(1L);
        cart.setUser(user);
        cart.setItems(items);
        cart.setTotal(item.getPrice());

        user.setCart(cart);

        UserOrder userOrder = new UserOrder();
        userOrder.setUser(user);
        userOrder.setItems(items);
        userOrder.setTotal(item.getPrice());
        userOrder.setId(1L);
        userOrder.setTotal(item.getPrice());


        List<UserOrder> listUserOrder = new ArrayList<>();
        listUserOrder.add(userOrder);


        when(userRepo.findByUsername("Username1234")).thenReturn(user);
        when(userRepo.findById(1L)).thenReturn(Optional.of(user));
        when(orderRepo.findByUser(user)).thenReturn(listUserOrder);


    }

    @Test
    public void testSubmit() throws Exception {
        final ResponseEntity<UserOrder> response = orderController.submit("Username1234");
        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());

        UserOrder uo = response.getBody();
        assertNotNull(uo);
    }

    @Test
    public void negative_test_submit() throws Exception {
        final ResponseEntity<UserOrder> response = orderController.submit("non_existence_user");
        assertNotNull(response);
        assertEquals(404, response.getStatusCodeValue());
    }

    @Test
    public void testGetOrdersForUser() throws Exception {
        final ResponseEntity<List<UserOrder>> response = orderController.getOrdersForUser("Username1234");

        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());

        List<UserOrder> luo = response.getBody();
        assertNotNull(luo);
        assertEquals(1, luo.size());
        assertEquals(1, luo.get(0).getItems().size());
    }


}
