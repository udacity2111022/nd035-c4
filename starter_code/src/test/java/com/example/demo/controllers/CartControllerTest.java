package com.example.demo.controllers;

import com.example.demo.TestUtils;
import com.example.demo.model.persistence.Cart;
import com.example.demo.model.persistence.Item;
import com.example.demo.model.persistence.User;
import com.example.demo.model.persistence.repositories.CartRepository;
import com.example.demo.model.persistence.repositories.ItemRepository;
import com.example.demo.model.persistence.repositories.UserRepository;
import com.example.demo.model.requests.CreateUserRequest;
import com.example.demo.model.requests.ModifyCartRequest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CartControllerTest {

    private CartController cartController;

    private UserRepository userRepo = mock(UserRepository.class);

    private CartRepository cartRepo = mock(CartRepository.class);

    private ItemRepository itemRepo = mock(ItemRepository.class);


    @Before
    public void setUp() {
        cartController = new CartController();
        TestUtils.injectObjects(cartController, "userRepository", userRepo);
        TestUtils.injectObjects(cartController, "cartRepository", cartRepo);
        TestUtils.injectObjects(cartController, "itemRepository", itemRepo);

        User user = new User();
        user.setId(1L);
        user.setUsername("Username1234");
        user.setPassword("Username1234");

        Item item = new Item();
        item.setId(1L);
        item.setName("item1");
        item.setDescription("hello_item");
        item.setPrice(BigDecimal.valueOf(1));

        List<Item> items = new ArrayList<>();
        items.add(item);

        Cart cart = new Cart();
        cart.setId(1L);
        cart.setUser(user);
        cart.setItems(items);
        cart.setTotal(item.getPrice());

        user.setCart(cart);

        when(userRepo.findByUsername("Username1234")).thenReturn(user);
        when(userRepo.findById(1L)).thenReturn(Optional.of(user));
        when(itemRepo.findById(1L)).thenReturn(Optional.of(item));
    }

    @Test
    public void create_cart_happy_path() throws Exception {
        ModifyCartRequest r = new ModifyCartRequest();
        r.setItemId(1L);
        r.setUsername("Username1234");
        r.setQuantity(1);

        final ResponseEntity<Cart> response = cartController.addTocart(r);

        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());

        Cart c = response.getBody();
        assertNotNull(c);
        assertEquals("item1", c.getItems().get(0).getName());
    }

    @Test
    public void negative_test_no_user() throws Exception {
        ModifyCartRequest r = new ModifyCartRequest();
        r.setItemId(1L);
        r.setUsername("non_exist_user");
        r.setQuantity(1);

        final ResponseEntity<Cart> response = cartController.addTocart(r);

        assertNotNull(response);
        assertEquals(404, response.getStatusCodeValue());
    }

    @Test
    public void testRemoveFromCart() throws Exception {
        ModifyCartRequest r = new ModifyCartRequest();
        r.setItemId(1L);
        r.setUsername("Username1234");
        r.setQuantity(1);

        final ResponseEntity<Cart> response = cartController.removeFromcart(r);
        assertNotNull(response);
        assertEquals(200, response.getStatusCodeValue());

        Cart c = response.getBody();
        assertNotNull(c);
        assertEquals(0, c.getItems().size());
    }
}
